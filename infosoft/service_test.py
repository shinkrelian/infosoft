import unittest

import xmlrunner as xmlrunner

from infosoft.service import get_year


class TestService(unittest.TestCase):
    def test_get_year(self):
        self.assertEqual(get_year(), 2016)

if __name__ == '__main__':
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'))
