from django.http import HttpResponse

from infosoft.service import get_year


def index(request):
    return HttpResponse(
        "<h1>Bienvenido a INFOSOFT {} <h1>".format(get_year()))

